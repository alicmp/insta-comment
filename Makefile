##
# RB-API
#
# @file
# @version 0.1

makemigrations:
	docker-compose run --rm app sh -c "python manage.py makemigrations"
	
migrate:
	docker-compose run --rm app sh -c "python manage.py migrate"

superuser:
	docker-compose run --rm app sh -c "python manage.py createsuperuser"

collectstatic:
	docker-compose run --rm app sh -c "python manage.py collectstatic"

run_test:
	docker-compose run --rm app sh -c "python manage.py test"

# end
