# Instagram Comment Bot
With this project you can handle sending automatic comment whenever someone
send a new post in instagram.
## Requirements
- Docker!
## Setup
In order to run this project first you have to install docker and 
docker-compose on your system, after that everything is going to work by simply
run this command:
```
cd <PROJECT DIRECTORY>
docker-compose up --build
```
Then open your browser and visit `localhost:8000/dashboard`.

After visiting your web dashboard make sure to:
- Insert your Instagram credentials
- Username of users which you want to send comment to
- Comment text which you want to send

and voila! Bot run almost every 60 minutes and send comment to those users who
sent new posts.