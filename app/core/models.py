from django.db import models


class MyInstagramUser(models.Model):
    username = models.CharField(max_length=255)
    password = models.CharField(max_length=255)

    def __str__(self):
        return self.username


class InstagramUser(models.Model):
    username = models.CharField(max_length=255)

    def __str__(self):
        return self.username


class Post(models.Model):
    user = models.ForeignKey('InstagramUser', on_delete=models.CASCADE)
    post_id = models.CharField(max_length=255)

    def __str__(self):
        return self.post_id


class Comment(models.Model):
    text = models.TextField()

    def __str__(self):
        return self.text