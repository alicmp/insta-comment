import time
import sys

from django.core.management.base import BaseCommand, CommandError
from core.models import MyInstagramUser, InstagramUser, Post, Comment
from core.utils.instagram import InstagramHandler


sys.stdout = open('report.log', 'w')


class Command(BaseCommand):
    help = 'Send message to latest users post'

    def handle(self, *args, **options):
        my_user = MyInstagramUser.objects.first()
        if not my_user:
            print('you have to enter your username and password')
            raise CommandError(
                'you have to enter your username and password')
        
        handler = InstagramHandler(my_user.username, my_user.password)
        
        users = InstagramUser.objects.all()
        if not users.exists():
            print('you have to add at least one instagram user')
            raise CommandError('you have to add at least one instagram user')
        
        for user in users:
            user_id = handler.get_user_id(user.username)
            last_post_id = handler.get_last_post_media_id(user_id)

            if Post.objects.filter(user=user, post_id=last_post_id).exists():
                continue
            Post.objects.create(user=user, post_id=last_post_id)

            comment = Comment.objects.order_by('?').first()
            if not comment:
                print('you have to enter some comment text')
                raise CommandError('you have to enter some comment text')
            handler.send_comment(last_post_id, comment.text)
            
            print(f'Successfully sent message {comment} to user {user.username}')
            time.sleep(60)

