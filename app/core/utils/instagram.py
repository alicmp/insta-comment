from InstagramAPI import InstagramAPI


class InstagramHandler:
    
    def __init__(self, username, password):
        self.api = InstagramAPI(username, password)
        self.api.login()


    def get_user_id(self, username):
        self.api.searchUsername(username) 
        try:
            return self.api.LastJson["user"]["pk"]
        except Exception:
            print("username doesn't exist") 
            return False
    

    def get_last_post_media_id(self, username_id):
        res = self.api.getUserFeed(username_id)

        if res is True:
            feed = self.api.LastJson
            if len(feed['items']) > 0:
                last_media = feed['items'][0]
                last_media_id = last_media.get('pk', None)
                return last_media_id
        
        return None
    

    def send_comment(self, mdeia_id, comment_text):
        response = self.api.comment(mdeia_id, comment_text)
        return response