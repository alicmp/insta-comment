from django.urls import path

from dashboard import views

urlpatterns = [
    path('', views.dashboard, name='dashboard'),

    path('logs/', views.show_logs, name='show-logs'),

    path('myuser/create/',
         views.MyInstagramUserCreate.as_view(),
         name='my-instagram-user-create'),
    path('myuser/delete/',
         views.MyInstagramUserDelete.as_view(),
         name='my-instagram-user-delete'),
    
    path('instagramuser/create/',
         views.InstagramUserCreate.as_view(),
         name='instagram-user-create'),
    path('instagramuser/<int:pk>/delete/',
         views.InstagramUserDelete.as_view(),
         name='instagram-user-delete'),
    
    path('comment/create/',
         views.CommentCreate.as_view(),
         name='comment-create'),
    path('comment/<int:pk>/delete/',
         views.CommentDelete.as_view(),
         name='comment-delete'),
]

app_name = 'dashboard'