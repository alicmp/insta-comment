import os
import threading
from django.shortcuts import render, redirect
from django.views.generic.edit import CreateView, DeleteView
from django.contrib import messages

from core import models as core_models


def dashboard(request):
    my_user = core_models.MyInstagramUser.objects.first()
    other_users = core_models.InstagramUser.objects.all()
    comments = core_models.Comment.objects.all()

    context = {
        'my_user': my_user,
        'other_users': other_users,
        'comments': comments,
    }
    return render(request, 'dashboard/dashboard.html', context)


def show_logs(request):
    if not os.path.exists('./report.log'):
        file_content = ''
    else:
        with open('./report.log', 'r') as f:
            file_content = f.read()
    return render(request, 'dashboard/logs.html', {'logs': file_content})


class MyInstagramUserCreate(CreateView):
    model = core_models.MyInstagramUser
    fields = ['username', 'password']
    template_name = 'dashboard/object_create.html'
    success_url = '/dashboard/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_title'] = 'My Instagram User:'
        return context


class MyInstagramUserDelete(DeleteView):
    model = core_models.MyInstagramUser
    template_name = 'dashboard/delete_confirmation.html'
    success_url = '/dashboard/'

    def get_object(self):
        return core_models.MyInstagramUser.objects.first()


class InstagramUserCreate(CreateView):
    model = core_models.InstagramUser
    fields = ['username']
    template_name = 'dashboard/object_create.html'
    success_url = '/dashboard/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_title'] = 'Instagram User:'
        return context


class InstagramUserDelete(DeleteView):
    model = core_models.InstagramUser
    template_name = 'dashboard/delete_confirmation.html'
    success_url = '/dashboard/'


class CommentCreate(CreateView):
    model = core_models.Comment
    fields = ['text']
    template_name = 'dashboard/object_create.html'
    success_url = '/dashboard/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_title'] = 'Comment:'
        return context


class CommentDelete(DeleteView):
    model = core_models.Comment
    template_name = 'dashboard/delete_confirmation.html'
    success_url = '/dashboard/'